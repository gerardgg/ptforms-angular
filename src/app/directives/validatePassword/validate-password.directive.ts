import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appValidatePassword]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePasswordDirective,
    multi: true
  }]
})
export class ValidatePasswordDirective {

  constructor() { }

  validate(passwordField: AbstractControl): {[key: string]: any} {
    let passwdLength = false;
    let passwdUpperAndLowerChars = false;
    if(passwordField?.value) {
      let passwdArray: string[] = passwordField.value.split('');
      
      let upperPasswdFlag = false;
      let lowerPasswdFlag = false;
  
      for(let i = 0; i < passwdArray.length; i++) {
        let currChar = passwdArray[i];
        if(currChar == currChar.toUpperCase()) {
          upperPasswdFlag = true;
        } else if(currChar == currChar.toLowerCase()) {
          lowerPasswdFlag = true;
        }
      }
  
      if(!upperPasswdFlag || !lowerPasswdFlag) {
        passwdUpperAndLowerChars = true;
      }

      if(passwordField.value.length <= 5) {
        passwdLength = true;
      }
    }
    return {'length': passwdLength, 'upperAndLower': passwdUpperAndLowerChars};
  }
}
