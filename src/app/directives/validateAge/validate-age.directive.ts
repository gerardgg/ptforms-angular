import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
@Directive({
  selector: '[appValidateAge]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateAgeDirective,
    multi: true
  }]
})
export class ValidateAgeDirective {

  constructor() { }
  validate(ageField: AbstractControl): { [key: string]: any } {
    let validateAgeField = false;
    if(ageField?.value){
      if (ageField.value < 18 || ageField.value >=100) {
        validateAgeField = true;
      }
    }
   
    return { 'ageError': validateAgeField }
  }
}