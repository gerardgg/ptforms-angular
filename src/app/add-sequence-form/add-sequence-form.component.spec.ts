import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSequenceFormComponent } from './add-sequence-form.component';

describe('AddSequenceFormComponent', () => {
  let component: AddSequenceFormComponent;
  let fixture: ComponentFixture<AddSequenceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSequenceFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSequenceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
