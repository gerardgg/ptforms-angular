import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { ValidateEmailDirective } from './directives/validateEmail/validate-email.directive';
import { ValidatePasswordDirective } from './directives/validatePassword/validate-password.directive';
import { ValidateAgeDirective } from './directives/validateAge/validate-age.directive';
import { AddSequenceFormComponent } from './add-sequence-form/add-sequence-form.component';
import { ValidateSequenceDirective } from './directives/validateSequence/validate-sequence.directive';

@NgModule({
  declarations: [
    AppComponent,
    RegisterFormComponent,
    LoginFormComponent,
    HomepageComponent,
    HeaderComponent,
    ValidateEmailDirective,
    ValidatePasswordDirective,
    ValidateAgeDirective,
    AddSequenceFormComponent,
    ValidateSequenceDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
